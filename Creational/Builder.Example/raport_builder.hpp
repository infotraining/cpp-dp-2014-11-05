#ifndef RAPORT_BUILDER_HPP
#define RAPORT_BUILDER_HPP

#include <string>
#include <vector>
#include <memory>

typedef std::vector<std::string> DataRow;

typedef std::string HtmlDocument;

typedef std::vector<std::string> CsvDocument;

class RaportBuilder
{
public:
	RaportBuilder(void) {}
	virtual RaportBuilder& add_header(const std::string& header_text) = 0;
	virtual RaportBuilder& begin_data() = 0;
	virtual RaportBuilder& add_row(const DataRow& data_row) = 0;
	virtual RaportBuilder& end_data() = 0;
	virtual RaportBuilder& add_footer(const std::string& footer) = 0;

	virtual ~RaportBuilder() {}
};


class HtmlRaportBuilder : public RaportBuilder
{
public:
	virtual RaportBuilder& add_header( const std::string& header_text );
	virtual RaportBuilder& begin_data();
	virtual RaportBuilder& add_row( const DataRow& data_row );
	virtual RaportBuilder& end_data();
	virtual RaportBuilder& add_footer( const std::string& footer );

	HtmlDocument get_report();
private:
	HtmlDocument doc_;
};

class CsvRaportBuilder : public RaportBuilder
{
public:
	virtual RaportBuilder& add_header( const std::string& header_text );
	virtual RaportBuilder& begin_data();
	virtual RaportBuilder& add_row( const DataRow& data_row );
	virtual RaportBuilder& end_data();
	virtual RaportBuilder& add_footer( const std::string& footer );

	CsvDocument get_report();
private:
	CsvDocument doc_;
};

#endif // RAPORT_BUILDER_HPP



