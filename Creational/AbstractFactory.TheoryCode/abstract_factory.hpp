#ifndef ABSTRACT_FACTORY_HPP_
#define ABSTRACT_FACTORY_HPP_

#include <iostream>
#include <string>
#include <stdexcept>

class AbstractProductA;
class AbstractProductB;


// "AbstractFactory"
class AbstractFactory
{
public:
	virtual AbstractProductA* create_product_A() = 0;
	virtual AbstractProductB* create_product_B() = 0;
	virtual ~AbstractFactory() {}
};

// "AbstractProductA"
class AbstractProductA
{
public:
	virtual std::string description() = 0;
	virtual ~AbstractProductA() {}
};

// "AbstractProductB"
class AbstractProductB
{
public:
	virtual std::string description() = 0;
	virtual void interact(AbstractProductA* a) = 0;
	virtual ~AbstractProductB() {}
};

// "ProductA1"
class ProductA1 : public AbstractProductA
{
public:
	std::string description()
	{
		return std::string("ProductA1");
	}
};

// "ProductB1"
class ProductB1 : public AbstractProductB
{
public:
	std::string description()
	{
		return std::string("ProductB1");
	}
	void interact(AbstractProductA* a)
	{
		std::cout << this->description() << " interacts with "
				<< a->description() << std::endl;
	}
};

// "ProductA2"
class ProductA2 : public AbstractProductA
{
public:
	std::string description()
	{
		return std::string("ProductA2");
	}
};

// "ProductB2"
class ProductB2 : public AbstractProductB
{
public:
	std::string description()
	{
		return std::string("ProductB2");
	}
	void interact(AbstractProductA* a)
	{
		std::cout << this->description() << " interacts with "
				<< a->description() << std::endl;
	}
};

// "ConcreteFactory1"
class ConcreteFactory1 : public AbstractFactory
{
public:
	AbstractProductA* create_product_A()
	{
		return new ProductA1();
	}

	AbstractProductB* create_product_B()
	{
		return new ProductB1();
	}
};

// "ConcreteFactory2"
class ConcreteFactory2 : public AbstractFactory
{
public:
	AbstractProductA* create_product_A()
	{
		return new ProductA2();
	}

	AbstractProductB* create_product_B()
	{
		return new ProductB2();
	}
};

// "Client"
class Client
{
private:
	AbstractProductA* abstract_product_A_;
	AbstractProductB* abstract_product_B_;
	Client(const Client&);
	Client& operator=(const Client&);
public:
	Client(AbstractFactory* factory) :
		abstract_product_A_(0), abstract_product_B_(0)
	{
		if (!factory)
			throw std::invalid_argument("NULL value of pointer");

		try
		{
			abstract_product_A_ = factory->create_product_A();
			abstract_product_B_ = factory->create_product_B();
		}
		catch(const std::bad_alloc& e)
		{
			delete abstract_product_A_;
			throw;
		}
	}

	~Client()
	{
		delete abstract_product_A_;
		delete abstract_product_B_;
	}

	void Run()
	{
		abstract_product_B_->interact(abstract_product_A_);
	}
};

#endif /*ABSTRACT_FACTORY_HPP_*/
