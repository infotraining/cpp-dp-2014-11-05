#include <iostream>
#include <string>
#include "prototype.hpp"

using namespace std;

int main()
{
	ConcretePrototype1* p1 = new ConcretePrototype1("CP1");
	ConcretePrototype1* c1 = p1->clone();
	cout << "Cloned: " << c1->get_id() << endl;

	ConcretePrototype2* p2 = new ConcretePrototype2("CP2");
	ConcretePrototype2* c2 = p2->clone();
	cout << "Cloned: " << c2->get_id() << endl;

	Prototype* c3 = c1->clone();
	cout << "Another clone: " << c3->get_id() <<  endl;

	delete p1;
	delete c1;
	delete p2;
	delete c2;
	delete c3;
}
