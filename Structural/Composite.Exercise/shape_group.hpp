#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <list>
#include <memory>

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupujący kształty geometryczne
class ShapeGroup : public Shape
{
public:
    using ShapePtr = std::shared_ptr<Shape>;

    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& s : source.shapes_)
            shapes_.emplace_back(s->clone());
    }

    void draw() const override
    {
        for(const auto& s : shapes_)
            s->draw();
    }

    void move(int dx, int dy) override
    {
        for(const auto& s : shapes_)
            s->move(dx, dy);
    }

    ShapeGroup* clone() const override
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream& in) override
    {
        int count;
        in >> count;

        std::string type_identifier;

        for(int i = 0; i < count; ++i)
        {
            in >> type_identifier;
            auto shape = ShapeFactory::instance().create(type_identifier);
            shape->read(in);

            shapes_.emplace_back(shape);
        }
    }

    void write(std::ostream& out) override
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& s : shapes_)
            s->write(out);
    }

    void add(ShapePtr shape)
    {
        shapes_.push_back(shape);
    }

private:
    std::list<ShapePtr> shapes_;
};

}

#endif /*SHAPE_GROUP_HPP_*/
