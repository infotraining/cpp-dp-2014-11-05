#include <memory>
#include "coffeehell.hpp"

using namespace std;

void client(Coffee& coffee)
{
    std::cout << "Description: " << coffee.get_description() << "; Price: " << coffee.get_total_price() << std::endl;
    coffee.prepare();
}

class CoffeeBuilder
{
    CoffeePtr coffee_;
public:
    template <typename T>
    CoffeeBuilder& create_base()
    {
        coffee_ = make_shared<T>();
        return *this;
    }

    template <typename T>
    CoffeeBuilder& add()
    {
        coffee_ = make_shared<T>(coffee_);

        return *this;
    }

    CoffeePtr get_coffee()
    {
        return coffee_;
    }
};

int main()
{
    auto espresso = make_shared<Espresso>();
    auto cf = make_shared<Whipped>(make_shared<Whisky>(make_shared<Whisky>(espresso)));
    client(*cf);

    CoffeeBuilder cb;
    cb.create_base<Espresso>().add<Whisky>().add<Whipped>();
    client(*cb.get_coffee());
}
