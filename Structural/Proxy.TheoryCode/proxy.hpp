#ifndef PROXY_HPP_
#define PROXY_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <mutex>

// "Subject" 
class Subject
{
public:
	virtual void request() = 0;
    virtual ~Subject() = default;
};

// "RealSubject"
class RealSubject : public Subject
{
    std::string id_;
public:
    RealSubject(const std::string& id) : id_ {id}
	{
        std::cout << "RealSubject's creation - id: " << id_ << std::endl;
	}

    RealSubject(const RealSubject&) = delete;
    RealSubject& operator=(const RealSubject&) = delete;

	~RealSubject()
	{
		std::cout << "RealSubject's clean-up" << std::endl;
	}
	
    void request() override
	{
		std::cout << "Called RealSubject.request()" << std::endl;
	}
};

// "Proxy" 
class Proxy : public Subject
{
    std::unique_ptr<RealSubject> real_subject_;
    std::string id_;
    std::once_flag flag_ {};
public:
    Proxy(const std::string& id) : real_subject_{nullptr}, id_{id}
	{
		std::cout << "Proxy's creation" << std::endl;
	}

	void request()
	{       
        std::call_once(flag_, [this] { real_subject_.reset(new RealSubject(id_)); });

		real_subject_->request();
	}
};

#endif /*PROXY_HPP_*/
