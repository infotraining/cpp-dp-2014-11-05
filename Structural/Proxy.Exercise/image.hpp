#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <mutex>
#include "shape.hpp"

namespace Drawing
{

class Image : public ShapeBase
{
public:
	Image(int x = 0, int y = 0, const std::string& path = "default.img") : ShapeBase(x, y), path_(path), buffer_(NULL), size_(0)
	{
		load_to_buffer();
	}

	Image(const Image& img) : ShapeBase(img.point().x(), img.point().y()), path_(img.path_), buffer_(NULL), size_(img.size_)
	{
		buffer_ = new char[size_];
		std::copy(img.buffer_, img.buffer_+img.size_, buffer_);
	}

	~Image()
	{
		delete [] buffer_;
	}

	Image& operator=(const Image& img)
	{
		Image temp(img);
		swap(temp);

		return *this;
	}

	void swap(Image& img)
	{
		std::swap(path_, img.path_);
		std::swap(size_, img.size_);
		std::swap(buffer_, img.buffer_);

		swap_points(img);
	}

	void swap_points(Image& other)
	{
		Point pt = point();
		set_point(other.point());
		other.set_point(pt);
	}

	std::string path() const
	{
		return path_;
	}

	void set_path(const std::string& path)
	{
		Point pt = point();
		Image temp(pt.x(), pt.y(), path);
		swap(temp);
	}

	virtual void draw() const
	{
		std::cout << "Drawing an image at: " << point() << " " << buffer_ << std::endl;
	}

	virtual Image* clone() const
	{
		return new Image(*this);
	}

	virtual void read(std::istream& in)
	{
		Point pt;
		std::string path;

		in >> pt >> path;

		set_point(pt);
		set_path(path);
	}

	virtual void write(std::ostream& out)
	{
		out << "Image " << point() << " " << path() << std::endl;
	}

protected:
	void load_to_buffer()
	{
		std::cout << "Loading an image file " << path_ << "...\n";

		std::ifstream fin(path_.c_str(), std::ios_base::binary);

		if (!fin.is_open())
			throw std::runtime_error("File not found...");

		// odczyt dlugosci pliku
		fin.seekg (0, std::ios::end);
		int length_of_file = static_cast<int>(fin.tellg());

		if (length_of_file == -1)
			throw std::runtime_error("Input stream error");

		fin.seekg (0, std::ios::beg);

		// wczytanie bufora
		buffer_ = new char[length_of_file + 1];
		fin.read(buffer_, length_of_file);
		buffer_[length_of_file] = '\0';
		size_ = length_of_file + 1;
	}

private:
	std::string path_;
	char* buffer_;
	size_t size_;
};

// TODO: zaimplementowac proxy dla klasy Image
class ImageProxy : public ShapeBase
{
    mutable std::unique_ptr<Image> img_ = nullptr;
    std::string path_;
    mutable std::once_flag init_flag_;
public:
    ImageProxy(int x = 0, int y = 0, const std::string& path = "default.img")
        : ShapeBase{x, y}, path_{path}
    {}

    ImageProxy(const ImageProxy& source) : ShapeBase{source}, path_{source.path_}, img_{nullptr}
    {
        if (source.img_)
        {
            img_.reset(source.img_->clone());
            std::call_once(init_flag_, []{});
        }
    }

    void draw() const
    {
        std::call_once(init_flag_, [this]{ img_.reset(new Image{point().x(), point().y(), path_});});

        img_->draw();
    }

    void move(int dx, int dy) override
    {
        if (img_)
            img_->move(dx, dy);
        else
            ShapeBase::move(dx, dy);
    }

    ImageProxy* clone() const override
    {
        return new ImageProxy{*this};
    }

    void read(std::istream& in) override
    {
        Point pt;
        std::string path;

        in >> pt >> path;

        set_point(pt);
        set_path(path);
    }

    void write(std::ostream& out) override
    {
        out << "Image " << point() << " " << path_ << std::endl;
    }

    void set_path(const std::string& path)
    {
        if (img_)
            img_->set_path(path);
        else
            path_ = path;
    }

    void set_point(const Point& pt)
    {
        ShapeBase::set_point(pt);
    }
};

}

#endif
