#ifndef CSV_VISITOR_HPP_
#define CSV_VISITOR_HPP_

#include "shape.hpp"
#include "circle.hpp"
#include "line.hpp"
#include "rectangle.hpp"

namespace Drawing
{

class CsvWriterVisitor : public Visitor
{
	std::ofstream out;
public:
	CsvWriterVisitor(const std::string& filename) : out(filename.c_str())
	{}

	// TODO: implementacja metod wizytujących
};

}

#endif /*CSV_VISITOR_HPP_*/
