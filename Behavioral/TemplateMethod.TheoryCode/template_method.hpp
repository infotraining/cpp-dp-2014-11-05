#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Service
{
public:
    virtual void run()
    {
        std::cout << "Service::run()" << std::endl;
    }

    virtual ~Service() = default;
};

class NewService : public Service
{
public:
    void run() override
    {
        std::cout << "NewService::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;

    virtual void hook_method() {}

    virtual std::unique_ptr<Service> create_service()
    {
        return std::unique_ptr<Service>{new Service()};
    }

public:
	void template_method()
	{
		primitive_operation_1();
        auto srv = create_service();
        srv->run();
		primitive_operation_2();
        hook_method();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    void hook_method() override
    {
        std::cout << "Hook method" << std::endl;
    }

    std::unique_ptr<Service> create_service() override
    {
        return std::unique_ptr<Service>{new NewService()};
    }
};

#endif /*TEMPLATE_METHOD_HPP_*/
