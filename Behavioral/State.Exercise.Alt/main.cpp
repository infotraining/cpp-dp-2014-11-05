#include "bank_account.hpp"

NormalState BankAccount::NORMAL(0.05);
OverdraftState BankAccount::OVERDRAFT(0.15);

int main()
{
	BankAccount ba1(1);
	ba1.print_status();
	ba1.deposit(10000.0);
	ba1.print_status();
	ba1.withdraw(5000.0);
	ba1.print_status();
	ba1.pay_interest();
	ba1.print_status();
	ba1.withdraw(10000.0);
	ba1.print_status();
	ba1.pay_interest();
	ba1.withdraw(1000.0);
	ba1.print_status();
	ba1.deposit(7000.0);
	ba1.print_status();
}
