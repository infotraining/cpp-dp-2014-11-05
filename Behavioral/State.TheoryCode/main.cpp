#include "state.hpp"

int main()
{
	// Setup context in a state
	State* state = new ConcreteStateA();
	Context c(state);

	// Issue requests, which toggles state
	c.request();
	c.request();
	c.request();
	c.request();
}
