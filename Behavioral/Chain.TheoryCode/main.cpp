#include <iostream>
#include "chain.hpp"

using namespace std;

int main()
{
	// Setup Chain of Responsibility
    std::shared_ptr<Handler> h1 { new ConcreteHandler1() };
    std::shared_ptr<Handler> h2 { new ConcreteHandler2() };
    std::shared_ptr<Handler> h3 { new ConcreteHandler3() };
    h1->set_successor(h2);
    h2->set_successor(h3);

    // Generate and process request
    int requests[8] = { 2, 5, 14, 22, 198, 3, 27, 20 };

    for(const auto& r : requests)
    {
        h1->handle_request(r);
    }
}
