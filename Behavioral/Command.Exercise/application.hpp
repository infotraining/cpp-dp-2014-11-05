#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_

#include "document.hpp"
#include "command.hpp"
#include <map>
#include <boost/algorithm/string.hpp>

class MenuItem
{
	std::string caption_;
    std::shared_ptr<Command> cmd_;

public:
    MenuItem(const std::string& caption, std::shared_ptr<Command> cmd)
		: caption_(caption), cmd_(cmd)
	{
	}

    void action()
	{
        std::cout << "Wykonano komende " << caption_ << "..." << std::endl;
		cmd_->execute();
	}

	~MenuItem()
	{
	}
};


class Application
{
public:
    std::map<std::string, std::unique_ptr<MenuItem>> menu_;

public:
    void add_menu(std::string name, std::unique_ptr<MenuItem> item)
	{
        menu_.insert(std::make_pair(boost::to_lower_copy(name), move(item)));
	}

	bool execute_action(const std::string& action_name)
	{
        auto menu_item = menu_.find(boost::to_lower_copy(action_name));

        if (menu_item != menu_.end())
		{
            menu_item->second->action();

			return true;
		}

		return false;
	}
};

#endif /* APPLICATION_HPP_ */
