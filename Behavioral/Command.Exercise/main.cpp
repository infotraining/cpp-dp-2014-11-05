#include "document.hpp"
#include "command.hpp"
#include "application.hpp"

#include <stack>
#include <string>

using namespace std;

class DocApplication : public Application
{
    Document doc_;
    CommandTracker cmd_tracker_;
public:
    DocApplication() : doc_("Wzorzec projektowy: Command")
    {
        // obiekty polecen
        unique_ptr<Command> cmd_paste(new PasteCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_undo(new UndoCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_print(new PrintCmd(doc_));
        unique_ptr<Command> cmd_toupper(new ToUpperCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_tolower(new ToLowerCmd(doc_, cmd_tracker_));
        unique_ptr<Command> cmd_copy(new CopyCmd(doc_));
        unique_ptr<Command> cmd_addtext(new AddTextCmd(doc_, cmd_tracker_));
        // TODO: ToLowerCmd
        // TODO: AddTextCmd

        // przyciski
        unique_ptr<MenuItem> menu_paste(new MenuItem("Paste", move(cmd_paste)));
        unique_ptr<MenuItem> menu_toupper(new MenuItem("ToUpper", move(cmd_toupper)));
        unique_ptr<MenuItem> menu_undo(new MenuItem("Undo", move(cmd_undo)));
        unique_ptr<MenuItem> menu_print(new MenuItem("Print", move(cmd_print)));
        unique_ptr<MenuItem> menu_tolower(new MenuItem("ToLower", move(cmd_tolower)));
        unique_ptr<MenuItem> menu_copy(new MenuItem("Copy", move(cmd_copy)));
        unique_ptr<MenuItem> menu_addtext(new MenuItem("AddText", move(cmd_addtext)));


        add_menu("Paste", move(menu_paste));
        add_menu("ToUpper", move(menu_toupper));
        add_menu("Print", move(menu_print));
        add_menu("Undo", move(menu_undo));
        add_menu("AddText", move(menu_addtext));
        add_menu("ToLower", move(menu_tolower));
    }
};

int main()
{
    DocApplication app;

    const std::string exit_application = "end";

	std::string cmd;
	do
	{
		std::cout << "Podaj komende: ";
		std::cin >> cmd;
		app.execute_action(cmd);
    } while (cmd != exit_application);

	std::cout << "Koniec pracy..." << std::endl;
}

